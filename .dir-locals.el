((nil . ((my+fh/project-name . "metapensiero.sqlalchemy.proxy")
         (my+fh/project-license . "GNU General Public License version 3 or later")))
 (python-mode . ((pycov-coverage-file . "coverage.json"))))
