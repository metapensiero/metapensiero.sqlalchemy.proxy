.. -*- coding: utf-8 -*-
.. :Project:   metapensiero.sqlalchemy.proxy -- ORM functionalities
.. :Created:   ven 30 dic 2016 18:57:17 CET
.. :Author:    Lele Gaifax <lele@metapensiero.it>
.. :License:   GNU General Public License version 3 or later
.. :Copyright: © 2016, 2017 Lele Gaifax
..

ORM queries
===========

.. automodule:: metapensiero.sqlalchemy.proxy.orm

.. autoclass:: ProxiedEntity
   :members:
   :special-members: __init__
