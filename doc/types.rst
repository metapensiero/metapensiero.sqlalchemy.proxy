.. -*- coding: utf-8 -*-
.. :Project:   metapensiero.sqlalchemy.proxy -- Types related functions
.. :Created:   sab 22 lug 2017 17:34:47 CEST
.. :Author:    Lele Gaifax <lele@metapensiero.it>
.. :License:   GNU General Public License version 3 or later
.. :Copyright: © 2017 Lele Gaifax
..

Types related functions
=======================

.. automodule:: metapensiero.sqlalchemy.proxy.types
   :members:
