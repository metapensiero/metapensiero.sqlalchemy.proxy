.. -*- coding: utf-8 -*-
.. :Project:   metapensiero.sqlalchemy.proxy -- Utilities
.. :Created:   ven 30 dic 2016 19:40:38 CET
.. :Author:    Lele Gaifax <lele@metapensiero.it>
.. :License:   GNU General Public License version 3 or later
.. :Copyright: © 2016, 2017 Lele Gaifax
..

Utility functions
=================

.. automodule:: metapensiero.sqlalchemy.proxy.utils
   :members:
